var chai = require('chai');
var expect =  chai.expect;
var assert = chai.assert;
var should = chai.should();
var Browser = require('zombie');

var url  =  "http://localhost:9001";

describe('iTrainingLTD.co.uk website ', function(){
    before(function(){
         this.browser = new Browser({site:url, debug:false});
     })

    describe('Home page', function(){

             it ('should show featured course in the home page', function(done){
                var browser = this.browser;
                browser.visit('/');
                browser.wait(function(){
                    expect(browser.text(".featured h3")).to.equal('FEATURED COURSE');
                    done();
                })
             })
    }) // Home page

      describe('Course Page', function(){
        it ('should load the course page', function(done){
            var browser = this.browser;
                browser.clickLink(browser.querySelectorAll('a')[1]);
                browser.wait(function(){
                    expect(browser.text(".courses h4")).to.equal('COURSE LIST');
                                       done();
                })
        })

        it ('should load nine (9) course list', function(done){
            var browser = this.browser;
                browser.clickLink(browser.querySelectorAll('a')[1]);
                browser.wait(function(){
                    expect(browser.queryAll(".courses .course-entry")).to.have.length(9);
                    done();
                })
        })





    }) // courses

})
