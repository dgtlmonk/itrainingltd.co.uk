<?php
$errors = array(); // array to hold validation errors
$data = array(); // array to pass back data
// validate the variables ======================================================
if (empty($_POST['firstname']))
$errors['name'] = 'Name is required.';
if (empty($_POST['message']))
$errors['message'] = 'message is required.';
if (empty($_POST['email']))
$errors['email'] = 'email is required.';

// return a response ===========================================================
// response if there are errors
if ( ! empty($errors)) {
  // if there are items in our errors array, return those errors
  $data['success'] = false;
  $data['errors'] = $errors;
  $data['messageError'] = 'Please check the fields in red';
} else {
  // if there are no errors, return a message
  $data['success'] = true;
  $data['messageSuccess'] = 'Thanks for reaching out. We will get back to you soon';
  // CHANGE THE TWO LINES BELOW
  $email_to = "mjec2001@yahoo.com";// "dgtlmonk@gmail.com";
  $email_subject = "iTrainingltd Inquiry Submission";
  $name = $_POST['firstname']; // required
  $contact = $_POST['number'];  // contact
  $title = $_POST['title']; // title
  $email_from = $_POST['email']; // required
  $message = $_POST['message']; // required
  $email_message = "Form Inquiry message below:";
  $email_message .= "\n\nTitle: ".$title;
  $email_message .= "\nName: ".$name;
  $email_message .= "\nContact: ".$contact;
  $email_message .= "\nEmail: ".$email_from;
  $email_message .= "\n\nMessage: \n ".$message;
  $headers = 'From: '.$email_from.
  'Reply-To: '.$email_from."rn" .
  'X-Mailer: PHP/' . phpversion();
  @mail($email_to, $email_subject, $email_message, $headers);
}
// return all our data to an AJAX call
echo json_encode($data);
