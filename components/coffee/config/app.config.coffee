# define our module
angular.module 'itraining', ['ui.router','kendo.directives','ngMaterial','ngMessages']

### @ngInject###

angular.module('itraining').config ($stateProvider, $urlRouterProvider) ->
        $urlRouterProvider.otherwise '/'
        $stateProvider
            .state 'home', {
                url:''
                templateUrl:'partials/home.html'
            }
            .state 'courses', {
                url: '/courses'
                controllerAs: 'courses'
                controller:'CoursesController'
                templateUrl:'partials/courses.html'
               
            }
            .state 'courses.detail', {
                url:'/:base/:courseId'
                controller:'CoursesController'

                # controller: ['$scope', ($scope) ->
                #     $scope.isDetail = true
                #     return
                # ]
                'views': {
                    '':{
                        template: ''
                      }
                    'course-detail': {
                         templateUrl: ($stateParams) -> #'partials/courses/' + + '.html'
                            'partials/courses/' + $stateParams.courseId + '.html' 
                    }
                }
                  
            }
            .state 'faq', {
                url: '/faq'
                templateUrl:'partials/faq.html'
            }
            .state 'about', {
                url: '/about'
                templateUrl:'partials/about.html'
            }
            .state 'contact', {
                url: '/contact'
                controller:'ContactCtr'
                templateUrl:'partials/contact.html'
            }
        return
    
