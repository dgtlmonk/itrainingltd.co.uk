# README #
itrainingltd.co.uk

### What is this repository for? ###

* SAP Site for a medical practitioner
* Version 1.0.2

### How do I get set up? ###

* (sudo) npm install

### Contribution guidelines ###

* mocha --reporter spec

### Who do I talk to? ###

* dgtlmonk@gmail.com / joel.pablo@digitalmonkstudio.com
