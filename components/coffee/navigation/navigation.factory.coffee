angular.module('itraining').factory 'NavigationFactory', ->
        navigation = {}
        navigation.navigationList = [
            { label:'home', url:''}
            { label:'courses', url:'#/courses'}
            { label:'faq', url:'#/faq'}
            { label:'about', url:'#/about'}
            { label:'contact', url:'#/contact'}
        ]
        navigation
