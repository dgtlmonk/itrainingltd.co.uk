# define our module
angular.module 'itraining', ['ui.router','kendo.directives','ngMaterial','ngMessages']

### @ngInject###

angular.module('itraining').config ($stateProvider, $urlRouterProvider) ->
        $urlRouterProvider.otherwise '/'
        $stateProvider
            .state 'home', {
                url:''
                templateUrl:'partials/home.html'
            }
            .state 'courses', {
                url: '/courses'
                controllerAs: 'courses'
                controller:'CoursesController'
                templateUrl:'partials/courses.html'
               
            }
            .state 'courses.detail', {
                url:'/:base/:courseId'
                controller:'CoursesController'

                # controller: ['$scope', ($scope) ->
                #     $scope.isDetail = true
                #     return
                # ]
                'views': {
                    '':{
                        template: ''
                      }
                    'course-detail': {
                         templateUrl: ($stateParams) -> #'partials/courses/' + + '.html'
                            'partials/courses/' + $stateParams.courseId + '.html' 
                    }
                }
                  
            }
            .state 'faq', {
                url: '/faq'
                templateUrl:'partials/faq.html'
            }
            .state 'about', {
                url: '/about'
                templateUrl:'partials/about.html'
            }
            .state 'contact', {
                url: '/contact'
                controller:'ContactCtr'
                templateUrl:'partials/contact.html'
            }
        return
    

### @ngInject###
angular.module('itraining').controller 'ContactCtr', ($scope, ContactSvc, $http)->
  $scope.contactData = {}
  $scope.submitted = false
  $scope.onSubmit = ()->
    params = {
      'title'    : $scope.contactData.title
      'firstname': $scope.contactData.firstname
      'email': $scope.contactData.email
      'lastname' : $scope.contactData.lastname || 'Not specified'
      'contact_number' : $scope.contactData.number || 'Not specified'
      'message'        : $scope.contactData.message
    }

    console.log 'params: ', param($scope.contactData)

    $http({  
    method : 'POST'
    url : 'http://itrainingltd.co.uk/forms/contact.php'
    data : param ($scope.contactData)
    headers : { 'Content-Type': 'application/x-www-form-urlencoded'}
      }).success( (res)->
        console.log 'form sent check email'
        console.log 'res: ', res
        $scope.submitted = true
       
        return
     
    )
    return 

  ### helper ###
  `
  var param = function (data)  {
    var returnString = '';
        for (d in data){
            if (data.hasOwnProperty(d))
               returnString += d + '=' + data[d] + '&';
        }
        // Remove last ampersand and return
    return returnString.slice( 0, returnString.length - 1 );
  }`
  
  return



### @ngInject###
angular.module('itraining').service 'ContactSvc', ()->
  api = {
    sendEmail: (paylod)->
      console.log 'Sending email from service', payload
      return 
  }

  return api


### @ngInject###

angular.module('itraining').controller 'CoursesController', ($scope, CoursesFactory, $stateParams, $state, $rootScope)->
    vm = @
    vm.courseList = CoursesFactory.courseList

    vm.coursesOptions = {
        contentUrls : [
                null
                null
        ]
    }

    console.log 'vm course', vm.courseList

    $scope.courseId = $stateParams.courseId
    if $state.includes('courses.detail')
      $scope.isDetail = true
      console.log 'whoah!'
    else
      $scope.isDetail = false

    $rootScope.$on '$stateChangeSuccess', (e, toState, toParams, frmState, frmParams)->
      console.log 'State Change Success !', toParams
      if toParams.base 
        $scope.base = vm.courseList[toParams.base]
      #  console.log 'base should be: '
      return
    return



angular.module('itraining').factory 'CoursesFactory', ->
        courses = {}
        courses.courseList = [
          "COURSES"
          "Resuscitation"
          "Clinical Skills"
          "Emergency Care"
          "Critical Care"
          "Home Care"
          "Primary Care"
          "Healthcare Assistant (HCA) Training"
        ]
        courses

# app = angular.module 'itraining',['kendo.directives']

### NavigationController = ($scope) ->
  $scope.navReady = false
  vm = @
  vm.navigationList = [
    { label:'home', url:''}
    { label:'courses', url:'#/courses/0/0'}
    { label:'faq', url:'#/faq'}
    { label:'about', url:'#/about'}
    { label:'contact', url:'#/contact'}
    ]
  $scope.navReady = true
  returni ###

# NavigationController.$inject = ['$scope']

### @ngInject###

angular.module('itraining').controller 'NavigationController', ($scope) ->
  $scope.navReady = false
  vm = @
  vm.navigationList = [
    { label:'home', url:''}
    { label:'courses', url:'#/courses/0/0'}
    { label:'faq', url:'#/faq'}
    { label:'about', url:'#/about'}
    { label:'contact', url:'#/contact'}
    ]
  $scope.navReady = true
  return



MainNavigation = () ->
  {
    restrict: 'AC'
    ### @ngInject###
    controller : ($scope)->
      
   
     return
   
  
    ### @ngInject ###
    ###controller: ($scope) ->
        $scope.toggleNavigation = ->
          console.log 'toggle nav ?'
          _nav = $('ul.nav')
          $(_nav).toggle 'slow'
          return
        return ###
    }

angular.module('itraining').directive('mainNavigation', ->
  {
    restrict: 'AC'
    ### @ngInject###
    controller: ($scope)->
      $scope.toggleNavigation =->
        return
      return
  
  }
  
  

)

angular.module('itraining').factory 'NavigationFactory', ->
        navigation = {}
        navigation.navigationList = [
            { label:'home', url:''}
            { label:'courses', url:'#/courses'}
            { label:'faq', url:'#/faq'}
            { label:'about', url:'#/about'}
            { label:'contact', url:'#/contact'}
        ]
        navigation
