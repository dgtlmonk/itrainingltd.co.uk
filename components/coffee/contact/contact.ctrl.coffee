### @ngInject###
angular.module('itraining').controller 'ContactCtr', ($scope, ContactSvc, $http)->
  $scope.contactData = {}
  $scope.submitted = false
  $scope.onSubmit = ()->
    params = {
      'title'    : $scope.contactData.title
      'firstname': $scope.contactData.firstname
      'email': $scope.contactData.email
      'lastname' : $scope.contactData.lastname || 'Not specified'
      'contact_number' : $scope.contactData.number || 'Not specified'
      'message'        : $scope.contactData.message
    }

    console.log 'params: ', param($scope.contactData)

    $http({  
    method : 'POST'
    url : 'http://itrainingltd.co.uk/forms/contact.php'
    data : param ($scope.contactData)
    headers : { 'Content-Type': 'application/x-www-form-urlencoded'}
      }).success( (res)->
        console.log 'form sent check email'
        console.log 'res: ', res
        $scope.submitted = true
       
        return
     
    )
    return 

  ### helper ###
  `
  var param = function (data)  {
    var returnString = '';
        for (d in data){
            if (data.hasOwnProperty(d))
               returnString += d + '=' + data[d] + '&';
        }
        // Remove last ampersand and return
    return returnString.slice( 0, returnString.length - 1 );
  }`
  
  return


