/*jshint unused:true */

var gulp =  require('gulp'),
    connect = require('gulp-connect'),
    lr = require('gulp-livereload'),
    coffee = require('gulp-coffee'),
    compass = require('gulp-compass'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    // merge = require('merge-stream'),
    uglify = require('gulp-uglify'),
    ngAnnotate = require('gulp-ng-annotate'),
    coffeelint = require('gulp-coffeelint'),
    plumber = require('gulp-plumber');

var paths  = {
    app_index:['./app/index.html'],
    sass_src:['./components/sass/*.scss'],
    css_dest:'./app/css/',

    coffee_src:'./components/coffee/**/*.coffee',
    js_src:'./app/js/**/*.js',
    js_dest:'./app/js'
    }


var onErr = function (e) {
    console.log('Error has occured: ' + e);
}


gulp.task('connect',function(){
    connect.server({
        port:9001,
        root:['app'],
        livereload:true,
        browser: 'chrome',
    })
})


gulp.task('coffee', function(){
    return gulp.src(paths.coffee_src)
        // .pipe(coffeelint())
        // .pipe(coffeelint.reporter())
        .pipe(sourcemaps.init())
       .pipe(concat('app.coffee'))
       .pipe(gulp.dest('.')) // save intermediate file
       .pipe(coffee({
           bare: true, sourceMaps: false
       }))
       .pipe(sourcemaps.write('',{ addComment: false }))
       .pipe(gulp.dest(paths.js_dest));
})


// task is dependent on 'coffee' to be finished
// gulp.task('js', ['coffee'], function(){
gulp.task('js',  function(){
    gulp.src('./app/js/app.js')
    .pipe(sourcemaps.init())
    //.pipe(concat('app.min.js')) // concat is done via coffee
    .pipe(ngAnnotate({
        add:true 
    }))
    .pipe(uglify())
    .pipe(sourcemaps.write({ addComment: false }))
    .pipe(gulp.dest('./app/js'))
})

gulp.task('compass',function(){
    return gulp.src(paths.sass_src)
          .pipe(plumber({
              errorHandler: onErr
          }))
          .pipe(compass({
                sass: './components/sass',
                css:'app/css',
                require:['susy','breakpoint','sassy-buttons'],
          }))
          .on('error', function (err) {
                console.log('Error on Compass, fix it dont worry ;-) ') ;
          })
          .pipe(gulp.dest('./app/css'))
})

gulp.task('watch',function(){
    lr.listen(); // livereloading
    gulp.watch(paths.sass_src,['compass']);
    gulp.watch(paths.coffee_src,['coffee']);
    gulp.watch(paths.js_src,['js']);
    gulp.watch(['app/css/main.css', paths.app_index,'./app/partials/*.html','./app/js/*.js']).on('change', lr.changed);
})


gulp.task('default',['coffee','watch','connect']);
